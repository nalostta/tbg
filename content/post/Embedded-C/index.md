---
title: "Embedded C"
date: 2021-04-10T08:58:02+05:30
draft: false
image: "img/pawel-czerwinski-9anj7QWy-2g-unsplash.jpg"
---

I really find it funny when people say embedded C out loud as if it is a new language. 
It is not. The language is still c/c++, but built to run on an mcu rather than a pc. 
In essence, embedded programming is a series of guidelines/a discipline you follow when building for an mcu. 
So you're looking at memory, processing and power constraints, real-time behaviour etc and all of this without the luxury that most desktop OS'es provide.

Here are some of the things needed to know:

- Learn c or c++ in this case well enough. The learner needs to be comfortable in using pointers, arrays and structs to begin with.

- Learn the build process of the application that inputs the source code, outputs the binary/elf and what happens in between. Also about the scope of a variable and it's linkage.

- Macros in C and inline functions in c++ will save you from a load of trouble by adding context to the numbers/data or constants you will be working with.

- Storage classes and qualifiers are heavily used, can't do embedded stuff without it.

### This was the language part, now with embedded part:

- Determine your software tool chain which will depend on your preference, the mcu, etc.

- Learn a bit about about the architecture you're programming going to use libraries. Learn a lot about architecture if you're programming from scratch. Everything runs using registers, you know it, you own it.
 
- Unlike normal application you build for your console, in embedded your using your laptop to compile the program for another device (cross compiling). So you will also need a way to figure out how to upload/burn the program into the microcontroller, there's many programmers out there too, learn to program and debug here.
 

And Don't forget to have fun :) It can get a bit intensive trying to do so much, but if you love this, it'll all be worth it.

