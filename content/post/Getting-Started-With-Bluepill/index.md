---
title: "Getting Started With Bluepill"
date: 2021-03-30T23:30:58+05:30
draft: false
description: "A Quick guide on how to get started with your very own Bluepill"

image: "getting_started_cover.jpg"

tags: [
    "beginner",
    "getting started",
    "tutorial",
]

---


## Hardware Pre-Requisites:
So the first thing to get started is to acquire the following hardware.
You will definitely need:

	- Your Bluepill (stm32f103c8t6).
	
	- Some jumper wires (a.k.a) dupont wires.
	
	- Programmer or a USB-to-UART (serial to ttl) converter.
	
	
### Your life will become a lot easier if you have:
	
- Breadboard : 
	
	This actually should be a must-have. Since it used for making connections, you can still move ahead without this but nevertheless, it is a good investment.
- Multimeter : 
	
	Required to check whether the connections are working, so once again, if you don't have it, don't sweat.
- Debugger : 
	
	As the programs become larger are more complex, the importance of a debugger becomes more critical. So expend your time and money if you're in this for the long haul. There are blogs that make use of ST-Link V2 programmer and openocd server on the computer as a debugger so that's a good way to go.
			

	

	


