+++
title = "Arduino vs Bluepill"
date = 2021-03-28T20:32:51+05:30
draft = false
description = "So which one is better?"

tags = [
    "Arduino",
    "Arduino uno",
    "comparison",
]
image = "AVB.png"
+++

While the answer may sound a bit obvious, I assure you, this post is more than just who wins the battle.
Choosing a microcontroller for your project is no easy task. There are tons of factors to weigh in that make the choice of a controller more difficult.

If you just want the summary, Skip to the TL:DR section.

![The battle of microcontrollers](AVSB.png)

## Comparison

We will evaluate the 2 based on the following parameters:

- Processor potential
- Memory
- Peripherals
- Library Support
- Time-To-Code/Learning Curve

![All the comparison parameters](parameters_mmap.png)


### Processor potential
	
We're comparing against the Arduino UNO having ATMEL's (now MICROCHIP's) AVR family based ATMEGA328 vs the ARM's cortex-M3.

Cortex-M3 is a mid-density 32-bit design whereas the [Atmega328 is 8-bit*](https://stackoverflow.com/questions/31106537/avr-internal-data-bus-width).
Meaning that the data bus length of the cortex-m3 is bigger and thus supports computation and storage of a bigger data-sizes.

ARM's attempt to introduce a cost-effective mcu which maximized it's functionality while reducing the silicon-real estate gave rise to cortex-m3. In comparison with ARM's other ranges, it places on the lower half of the ladder. Despite all these, the cortex-M3 has a max clock speed of 72 MHZ and features a sleep mode, a complicated clocking configuration which enables you to power down any peripheral you don't want, to save power. 

### Memory

check out the table below:

![comparison table](AVSB_table.PNG)

The table and the official datasheet say 64KB of flash but in reality, the clones obtained from china markets have known to support double the size.
With almost 10x the SRAM and 128KB of flash, Bluepill obviously wins this round!
	
### TL:DR

 - Hardware-wise, arduino UNO loses terribly, the bluepill is better in every way. But then again, this is usually the case with ARM-based microcontrollers when compared to other architectures.
 - BluePill is cheaper, more powerful and debug support is awesome. (no seriously, SWD kills!)
 - The only consolation of using UNO over tbp is the fact that it has a better software support. The learning curve to "setup" an application by a complete noob is at a fraction of the time taken in the case of tbp.
	- However, this edge is lost once you become a fairly advanced programmer (no,not on arduino, get out of that please). 
	
##### So when do you use arduino?

  1. You are on a tight schedule and the task is simple.
  2. You are not very familiar with anything else.
 
##### ...And what about bluepill or any other microcontroller?
   Hardware requirements come first, you have to learn the relevant stuff based on the hardware you select. 
   However, if you are concerned about the software tool-chain, it is a matter of preference, convenience and how much time you are willing to give to learn it.
	
	
	
