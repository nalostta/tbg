---
title: "What is an Rtos and Why Do We Use It"
date: 2021-04-10T09:04:57+05:30
draft: false
image: "QNX-RTOS-illustration.jpg"
---

In general, an os abstracts you from scheduling. What does this mean?

Imagine say you have a task of reading some data from the sensor every 10 seconds. So you poll it all and read the data to begin with and add delays to get it to 10 seconds. 
Not a great way to implement but gets the job done.

Now say you have to check the serial line for incoming messages every 500 ms in addition to previous task, so you set up timers and interrupts. 
As soon as data comes in, interrupt fires and you read the data. 
Also the timer fires every 10 seconds to read the sensor data. Cool.

Let's say you now have to control the speed of a motor based on the number received from the serial communication setup in the prev case and also display it along with the sensor data on an LCD display. 
Here the sequencing/arrangement of code gets a bit complicated but not impossible (assuming the processor can handle the load). 
A bit of tweaking in the poll loop, add a couple more timers, check for flags to excute certain functions etc...still doable.. But you see where I'm going with this? 
Add 10 more sensors, peripherals, memory management, io etc all the works....now managing all the events and ensuring that those functions getting executed at the right time while juggling from multiple interrupt fires and their priorities becomes a programmers nightmare!!

Enter rtos, what you do here is you create a "task" which is a function dedicated to carry out a sole task. 
Say read from serial line and write it to a buffer. 
Another task to read sensor data and write it to another buffer. 
Another task that reads from all the buffers and writes to the display and so on for every task you write its function. 
Then you give these the handles of these "tasks" to the rtos scheduler along with their priority, frequency of execution etc and then start the rtos. 
The rtos orchestrates everything like that lead musician with the stick in a musical orchestra.

So what happened differently here? When you were writing the routines, you were not worrying about when to execute them or about the other tasks. 
It feels like each task is executing independently with all the resources. 
But we know better, the rtos does all the magic.

The thing differentiating an rtos from a general os is the "hard or soft real-time factor". 
When you click on refresh in chrome browser, you are waiting for the browser to reload whenever it may do and in certain cases it may even take forever, so the behaviour is essentially said to be "non-deterministic" in nature. 
Even if it loads a little late, nothing burns out or is destroyed. 
In industries where applications are time-critical in nature, you cannot always wait for or ignore important signals because some very important systems may be controlled by it. 
Here you need to define how the system will behave, and ensure that it will either respond or if it cannot, send the error/failure message as early as possible. 
It has to be "deterministic" in nature. That's where you use an RTOS.

That and being light weight so that it will fit within the memory constraints are the 2 things that differentiate an rtos from a normal os